import tkinter as tk


class App(tk.Tk):
    def __init__(self):
        super().__init__()
#####################
        tk.Label(self, text="Sensors diameters (in millimeters) separated by comma :").grid(row=0, column=0)
        self.entry_sxy = tk.Entry(self)
        self.entry_sxy.grid(row=0, column=1)

        tk.Label(self, text="Overlap percentage: %").grid(row=1, column=0)
        self.entry_overlap = tk.Entry(self)
        self.entry_overlap.grid(row=1, column=1)

        tk.Label(self, text="Focal length (in millimeters):").grid(row=2, column=0)
        self.entry_fl = tk.Entry(self)
        self.entry_fl.grid(row=2, column=1)

        tk.Label(self, text="Attitude (in meters): ").grid(row=3, column=0)
        self.entry_att = tk.Entry(self)
        self.entry_att.grid(row=3, column=1)

        #############################
        btn = tk.Button(self, text="Submit", command=self.on_submit)
        btn.grid(row=6, column=0, columnspan=2, sticky='ew')
        #######
        self.t_text1 = tk.StringVar()
        self.t_text1.set("")
        self.output1 = tk.Entry(self, state="readonly", textvariable=self.t_text1).grid(row=4 ,columnspan = 2)

        self.t_text2 = tk.StringVar()
        self.t_text2.set("")
        self.output2 = tk.Entry(self, state="readonly", textvariable=self.t_text2).grid(row=5, columnspan=2)



    def on_submit(self):
        try:
            s_data = self.entry_sxy.get().split(",")
            s_data = [float(s_data[0])* (10**-3),float(s_data[1]) * (10**-3)]


            ol_data = self.entry_overlap.get().split(",")
            ol_data = [float(ol_data[0]) /100, float(ol_data[1]) /100]

            f= float(self.entry_fl.get()) * (10**-3)
            n= float(self.entry_att.get())

            ls = s_data[0] * (n / f) * (1 - ol_data[0])
            sil = s_data[1] * (n / f) * (1 - ol_data[1])

            self.t_text1.set("Line Space={} ".format(ls))

            self.t_text2.set("Space In Line={}".format(sil))
        except:
            pass

##############################







'''

       #############################
        btn = tk.Button(self, text="Submit", command=self.on_submit)
        btn.grid(row=2, column=0, columnspan=2, sticky='ew')

##############################





###################
        self.t_text = tk.StringVar()
        self.t_text.set("")
        self.output = tk.Entry(self,state="readonly",textvariable=self.t_text).grid(row=1, column=1)
##########################




  
'''





if __name__ == "__main__":
    App().mainloop()